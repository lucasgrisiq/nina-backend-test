import os
import json
import boto3
import botocore
from http import HTTPStatus

dynamodb = boto3.client('dynamodb',
                        region_name=os.environ['DEFAULT_REGION'],
                        aws_access_key_id=os.environ['ACCESS_KEY_ID'],
                        aws_secret_access_key=os.environ['SECRET_ACCESS_KEY'])


# expected body format:
# {
#   _id: uuid str,
#   task: str,
#   priority: int
# }

def lambda_handler(event, context):

    todo = json.loads(event['body'])

    # clean up and validate
    todo = clean_up(todo)
    ok = validate(todo)
    if not ok:
        status_code = HTTPStatus.BAD_REQUEST
        return gen_response('',status_code)
    
    item = gen_item(todo)
    body = ''

    try:
        response = dynamodb.put_item(
            TableName='TodoTable',
            Item=item,
            ConditionExpression='attribute_not_exists(#id)',
            ExpressionAttributeNames={
                '#id': '_id'
            }
        )
        body = json.dumps(response)
        status_code = HTTPStatus.OK
    except botocore.exceptions.ClientError as error:
        raise error


    res = gen_response(body, status_code)

    return res

def clean_up(body):
    new_body = dict.fromkeys(('_id', 'task', 'priority'), '')
    for k in new_body.keys():
        new_body[k] = body[k] if k in body.keys() else ''
    
    return new_body

def validate(data):
    # check attributes
    if (data['priority'] and data['task'] and data['_id']):
        v = int(data['priority'])
        return True if (v >= 1 and v <= 3) else False 
    return False

def gen_response(body, http_code):
    h = {
        'isBase64Encoded': False,
        'statusCode': http_code,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': body
    }

    return h

def gen_item(data):
    return {
        '_id': { 'S': str(data['_id']) },
        'task': { 'S': str(data['task']) },
        'priority': { 'N': str(data['priority']) }
    }