import os
import json
import boto3
import botocore
from http import HTTPStatus

dynamodb = boto3.client('dynamodb',
                        region_name=os.environ['DEFAULT_REGION'],
                        aws_access_key_id=os.environ['ACCESS_KEY_ID'],
                        aws_secret_access_key=os.environ['SECRET_ACCESS_KEY'])

def lambda_handler(event, context):
    
    # retrieve all
    if not event['pathParameters']:
        try:
            response = dynamodb.scan(
                TableName='TodoTable',
                Select='ALL_ATTRIBUTES'
            )
            body = json.dumps(response)
            status_code = HTTPStatus.OK
            return gen_response(body, status_code)
        except botocore.exceptions.ClientError as error:
            raise error
    
    # retrieve one
    key = event['pathParameters']['id']
    body = ''
    try:
        response = dynamodb.get_item(
            TableName='TodoTable',
            Key={ '_id': { 'S': key } }
        )

        body = json.dumps(response)
        status_code = HTTPStatus.OK
    except botocore.exceptions.ClientError as error:
        raise error

    res = gen_response(body, status_code)

    return res

def gen_response(body, http_code):
    h = {
        'isBase64Encoded': False,
        'statusCode': http_code,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': body
    }

    return h

