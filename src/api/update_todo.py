import os
import json
import boto3
import botocore
from http import HTTPStatus

dynamodb = boto3.client('dynamodb',
                        region_name=os.environ['DEFAULT_REGION'],
                        aws_access_key_id=os.environ['ACCESS_KEY_ID'],
                        aws_secret_access_key=os.environ['SECRET_ACCESS_KEY'])

# expected body format:
# {
#   task: str | null,
#   priority: int | null
# }

def lambda_handler(event, context):
    
    todo = json.loads(event['body'])

    # clean up and validate
    todo = clean_up(todo)
    ok = validate(todo)
    if not ok:
        status_code = HTTPStatus.BAD_REQUEST
        return gen_response('', status_code)

    updates = get_att_values(todo)
    key = event['pathParameters']['id']

    try:
        response = dynamodb.update_item(
            TableName='TodoTable',
            Key={ '_id': { 'S': key } },
            AttributeUpdates=updates,
            ReturnValues="ALL_NEW"
        )
        body = json.dumps(response)
        status_code = HTTPStatus.OK
    except botocore.exceptions.ClientError as error:
        raise error
   
    res = gen_response(body, status_code)
    return res

def clean_up(body):
    new_body = dict.fromkeys(('task', 'priority'), '')
    for k in new_body.keys():
        new_body[k] = body[k] if k in body.keys() else ''
    
    return new_body

def validate(data):
    if data['priority']:
        value = int(data['priority'])
        return True if (value >= 1 and value <= 3) else False 
    return True

def gen_response(body, http_code):
    h = {
        'isBase64Encoded': False,
        'statusCode': http_code,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': body
    }

    return h

def get_att_values(data):
    updates = dict()
    if data['task']:
        updates['task'] = { 
            "Value": { 'S' : str(data['task']) },
            "Action": "PUT"
        }
    if data['priority']:
        updates['priority'] = { 
            "Value": { 'N' : str(data['priority']) },
            "Action": "PUT"
        }
    return updates
