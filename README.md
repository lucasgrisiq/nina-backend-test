# Nina Back-end Test

---

This project uses the [Serverless Framework](https://serverless.com), Python 3.7, AWS Lambda, AWS API Gateway and AWS DynamoDB.

---

## The Test

- Your task is to build and deploy on AWS an API with a CRUD of Todos using the Serverless Framework, Lambda functions and DynamoDB.

- At task completion, make a pull request to this repository with the API url at the end of the README.md

- You can use any language you like, just remember to make the proper changes to the configuration files

- The [Create function](/src/api/create_todo.py) is already implemented as an example, but you'll have to modify it to fit the requirements

- If you have any trouble to complete this task you can add comments to the *Problems* section of this README explaining your situation

- !!! Each todo object must have the following schema, and you have to **make sure** the API doesn't allow different objects and that it returns the proper HTTP codes
```py
# Todo Object description
# _id: identifier uuid string
# task: string 
# priority: int [1, 2, 3]

{
    '_id': uuid,
    'task': str,
    'priority': int
}
```

We encourage you to read the documentation of the tools used in this task:
- [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
- [Serverless Framework](https://www.serverless.com/framework/docs/)

## Environment

### AWS

You are required to create an [AWS account](https://aws.amazon.com/pt/) if you don't have one yet.

#### Setting Up AWS

1. Create AWS credentials including the following IAM policies: `AWSLambdaFullAccess`, `AmazonAPIGatewayAdministrator` and `AWSCloudFormationFullAccess`.
1. Set the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` variables in your environment.

### Environment Variables

Secrets are injected into your functions using environment variables. By defining variables in the provider section of the `serverless.yml` you add them to the environment of the deployed function. From there, you can reference them in your functions as well.

So you would add something like:
```yml
provider:
  environment:
    A_VARIABLE: ${env:A_VARIABLE}
```
to your `serverless.yml`, and then you can add `A_VARIABLE` to your code and it will get picked up and deployed with your function.

## Problems

## API url goes here
